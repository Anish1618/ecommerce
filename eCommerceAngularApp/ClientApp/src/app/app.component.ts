import { Component,OnInit,ViewContainerRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { } from 'rxjs';

@Component({
  selector: 'esh-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
